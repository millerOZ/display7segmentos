/**
 * 
 */
/**
 * @author mikler
 *
 */
package displaysegmentos;

import java.util.ArrayList;
import java.util.Scanner;

class Displaysegmentos{
	
	public static String [][] generarMatrizNumero(int num){
		
		String [][] matrizAux = new String[5][3];
		for(int fila = 0;fila < 5;fila++) {
			for(int col = 0; col < 3;col++) {
				matrizAux[fila][col] = " ";
			}
		}
		matrizAux[0][1] =          "─";
		matrizAux[1][0] = "|"; matrizAux[1][2] = "|";
		matrizAux[2][1] =          "─";
		matrizAux[3][0] = "|"; matrizAux[3][2] = "|";
		matrizAux[4][1] =          "─";
		if (num == 1) {
			matrizAux[0][1] = " ";
			matrizAux[1][0] = " ";
			matrizAux[2][1] = " ";
			matrizAux[3][0] = " ";
			matrizAux[4][1] = " ";
			return matrizAux;
		}else if(num ==2) {
			matrizAux[1][0] = " ";
			matrizAux[3][2] = " ";
			return matrizAux;
		}else if(num == 3) {
			matrizAux[1][0] = " ";
			matrizAux[3][0] = " ";
			return matrizAux;
		}else if(num == 4) {
			matrizAux[0][1] = " ";
			matrizAux[3][0] = " ";
			matrizAux[4][1] = " ";
			return matrizAux;
		}else if(num == 5) {
			matrizAux[1][2] = " ";
			matrizAux[3][0] = " ";
			return matrizAux;
		}else if(num == 6) {
			matrizAux[1][2] = " ";
			return matrizAux;
		}else if(num == 7) {
			matrizAux[1][0] = " ";
			matrizAux[2][1] = " ";
			matrizAux[3][0] = " ";
			matrizAux[4][1] = " ";
			return matrizAux;
		}else if(num == 9) {
			matrizAux[3][0] = " ";
			matrizAux[4][1] = " ";
			return matrizAux;
		}else if(num == 0) {
			matrizAux[2][1] = " ";
			return matrizAux;
		}
		return matrizAux;
	}
	
	public static void imprimeDisplay(int nveces,int numero) {
		if (nveces >0 && nveces <6) {
			String numeroStr  = Integer.toString(numero);
			char [] aux = numeroStr.toCharArray();
			
			ArrayList<String[][]> listaMatriznum = new ArrayList<String[][]>();
			
			if(aux.length < 6 && aux.length > 0) {
				for (int i = 0; i<aux.length;i++) {
					listaMatriznum.add(generarMatrizNumero(Character.getNumericValue(aux[i])));
				}
				concatenaNum(aux.length, listaMatriznum);
			}
		}
	}
	
	public static void concatenaNum(int nDigitos, ArrayList<String[][]> listaMatriznum) {
		
		String auxStr = "";
		String [] vectorFila = new String [3 * nDigitos];
		
		if(listaMatriznum.size() == 1) {
			for(int f = 0;f <5;f++) {
				for(int c = 0; c<3;c++) { 
					System.out.print(listaMatriznum.get(0)[f][c]);
				}
				System.out.println();
			}
		}else if(listaMatriznum.size() == 2) {
			for(int f = 0;f <5;f++) {
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(0)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(1)[f][c];}
				vectorFila[f] = auxStr;  auxStr = "";
			}
		}else if(listaMatriznum.size() == 3) {
			for(int f = 0;f <5;f++) {
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(0)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(1)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(2)[f][c];}
				vectorFila[f] = auxStr;  auxStr = "";
			}
		}else if(listaMatriznum.size() == 4) {
			for(int f = 0;f <5;f++) {
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(0)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(1)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(2)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(3)[f][c];}
				vectorFila[f] = auxStr;  auxStr = "";
			}
		}else {
			for(int f = 0;f <5;f++) {
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(0)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(1)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(2)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(3)[f][c];}
				for(int c = 0; c<3;c++) { auxStr += listaMatriznum.get(4)[f][c];}
				vectorFila[f] = auxStr;  auxStr = "";
			}
		}
		if(nDigitos != 1) {
			for (int i = 0; i < 5; i++) {
				System.out.println(vectorFila[i]);
			}
		}
	}
	public static void nvecesImprime(int veces,int numero) {
		for(int i =0; i < veces;i++) {
			imprimeDisplay(1,numero);
		}
	}
	
	public static void main(String[] args) {
		
		while(true) {
			try {
				Scanner in = new Scanner(System.in);
				String [] valoresEntrada = in.nextLine().split(",");
			
				int veces = Integer.parseInt(valoresEntrada[0]);
				int numero = Integer.parseInt(valoresEntrada[1]);
				
				String numeroStr  = Integer.toString(numero);
				
				if(veces == 0 && numero == 0) {
					System.out.println("Saliste del programa...");
					break;
				}else if (veces > 5 ) {
					System.out.println("max 5 repeticiones, vuelva a intentar -salir con (0,0)-");
					continue;
				}else if(numeroStr.toCharArray().length > 5) {
					System.out.println("Numero de max 5 digitos, vuelva a intentar salir con (0,0)");
					continue;
				}
				else {
					nvecesImprime(veces,numero);
				}
				
			} catch (Exception e) {
				System.out.println("valores numericos");
			}
			
		}
		
		
	}
}