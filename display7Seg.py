#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 13:38:58 2019

@author: Miller Ossa Samboni
"""
item1 = [[[" "],["─"],[" "]],
         [["|"],[" "],["|"]],
         [[" "],["─"],[" "]],
         [["|"],[" "],["|"]],
         [[" "],["─"],[" "]]]

def matrizNumero(item1,num): # recibe matriz y valores a modificar (eliminar char para formar numero) retorna numero que se pasa la diccionario
    item1 = [[[" "],["─"],[" "]],
             [["|"],[" "],["|"]],
             [[" "],["─"],[" "]],
             [["|"],[" "],["|"]],
             [[" "],["─"],[" "]]]
    aux = item1
    if ( num  == 1 ) :
        aux[0][1][0] =  " "
        aux[1][0][0] =  " "
        aux[2][1][0] =  " "
        aux[3][0][0] =  " "
        aux[4][1][0] =  " "
        return aux
    elif(num == 2):
        aux[1][0][0] =  " "
        aux[3][2][0] =  " "
        return aux
    elif(num == 3):
        aux[1][0][0] =  " "
        aux[3][0][0] =  " "
        return aux
    elif(num == 4):
        aux[0][1][0] =  " "
        aux[3][0][0] =  " "
        aux[4][1][0] =  " "
        return aux
    elif(num == 5):
        aux[1][2][0] = " "
        aux[3][0][0] = " "
        return aux
    elif(num == 6):
        aux[1][2][0] = " "
        return aux
    elif(num == 7):
        aux[1][0][0] = " "
        aux[2][1][0] = " "
        aux[3][0][0] = " "
        aux[4][1][0] = " "
        return aux
    elif(num == 9):
        aux[3][0][0] = " "
        aux[4][1][0] = " "
        return aux
    elif(num == 0):
        aux[2][1][0] = " "
        return aux
    else:
        return item1
    
diccionario = {
        0: matrizNumero(item1,0),
        1: matrizNumero(item1,1),
        2: matrizNumero(item1,2),
        3: matrizNumero(item1,3),
        4: matrizNumero(item1,4),
        5: matrizNumero(item1,5),
        6: matrizNumero(item1,6),
        7: matrizNumero(item1,7),
        8: matrizNumero(item1,8),
        9: matrizNumero(item1,9)
        }

def imprimeDisplay(nveces,numero):
    if (nveces > 0 and nveces < 6):
        listaNumero = list(map(int,list(str(numero))))
        if len(listaNumero) < 6:
            listaMatrizNum = []
            for num in range(len(listaNumero)):
                aux = listaNumero[num]
                listaMatrizNum.append(diccionario[aux])
            concatenaNum(len(listaNumero),listaMatrizNum)
#
def concatenaNum(ndigitos,listaMatrizNum):
    # crear variables para asignar a cada fila
    variable = 'ABCDE'
    auxVar = list(variable)
    lvar = []
    for i in range(ndigitos):
        lvar.append(auxVar[i])
    #asigno acada variables codigo del numero
    for k in range(ndigitos):
        lvar[k] = listaMatrizNum[k]
    #concateno caracteres de los numeros
    fil = []
    for k in range(5):
        if(len(lvar) == 2):
            fil.append(lvar[0][k]+lvar[1][k])
        elif(len(lvar) == 1):
            fil.append(lvar[0][k])
        elif(len(lvar) == 3):
            fil.append(lvar[0][k]+lvar[1][k]+lvar[2][k])
        elif(len(lvar) == 4):
            fil.append(lvar[0][k]+lvar[1][k]+lvar[2][k]+lvar[3][k])
        else:
            fil.append(lvar[0][k]+lvar[1][k]+lvar[2][k]+lvar[3][k]+lvar[4][k])
            
    for f in range(5):
        strinNum = ''
        for c in range(len(lvar)*3):
            strinNum += str(fil[f][c][0])
        print(strinNum)
        
def nvecesImprime(numeros):
    tamanionumero = list(map(int,list(str(numeros[1]))))
    for i in range(numeros[0]):
        imprimeDisplay(1,int(numeros[1]))
        
while True:
    try:
        entrada = input()
        numeros = list(map(int,entrada.split(',')))
        numeroDig = len(list(map(int,list(str(numeros[1])))))
        if(numeros[0] == 0 and numeros[1] == 0):
            break
        elif(numeros[0] > 5 or numeroDig > 5 ):
            entrada = input()
            numeros = list(map(int,entrada.split(',')))
            nvecesImprime(numeros)
        else:
            nvecesImprime(numeros)

    except ValueError:
        print("Ingresa un valor numerico")
    
