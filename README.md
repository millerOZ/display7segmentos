# Pseudocódigo display 7 segmentos
+ Se debe validar que el primer número este en el rango [1,5] y que el número siguiente tenga 5 dígitos
+ Se crea una matriz 5x3, donde se escribe el numero 8 en display 7 segmentos

![num8](num8.png)

+ Para generar los siguientes números se van remplazando los segmentos por espacios en blanco
+ Se crea una función que retorna una matriz con los segmentos de acuerdo al numero ingresado
+ Se almacenan en un diccionario los segmentos que corresponden a cada numero
+ Se crea un vector donde se almacena los segmentos de cada uno de los dígitos  del número ingresado, esto se logra pasando dígito por dígito al diccionario creado
+ Se llama a la función concatenaNum donde se pasa el vector creado que tiene lista se segmentos de cada numero, y la cantidad de dígitos  
+ En la función concatenaNum creamos una variable a cada matriz de segmentos
+ Se concatena los caracteres de los números, por filas, tamaño de la columnas depende de la cantidad de dígitos
+ Se imprime la matriz
